import React, {Component} from 'react';
import {initializeApp} from "@firebase/app";
import {getAuth, GoogleAuthProvider, signInWithPopup} from "@firebase/auth";
import {config} from '../Configs/FirebaseConfig'
import {toast} from "react-toastify";
import axios from "axios";

export const UserContext = React.createContext(true);

class UserProvider extends Component {

    constructor(props) {
        super(props)
        this.state = {
            firstname: '',
            lastname: '',
            email: null,
            imageUrl: null,
            isLoggedIn: true,
            jwtAccessToken: null,
            jwtRefreshToken: null
        }

        initializeApp(config);

        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
    }

    componentDidMount() {
        // getAuth().onAuthStateChanged(user => {
        //     if (!user) {
        //         this.logout();
        //     }
        // })
    }

    login() {
        const auth = getAuth();
        const provider = new GoogleAuthProvider();
        signInWithPopup(auth, provider)
            .then((result) => {
                let user = result.user;
                getAuth().currentUser.getIdToken(true).then(idToken => {
                    axios.post('http://localhost:8000/api/token', {'firebase-token': idToken})
                        .then(response => {
                            this.setState({
                                firstname: user.displayName.split(' ')[0],
                                lastname: user.displayName.split(' ')[1],
                                email: user.email,
                                imageURL: user.photoUrl,
                                isLoggedIn: true,
                                jwtAccessToken: response.data.accessToken,
                                jwtRefreshToken: response.data.refreshToken
                            })
                        })
                        .catch(e => {
                            console.log(e)
                            this.logout();
                        })
                })
                    .catch(e => {
                        console.log(e);
                        this.logout();
                    })
            }).catch((error) => {
            console.log(error.code, error.message, GoogleAuthProvider.credentialFromError(error));
            toast.error('Login with firebase failed.')
            this.logout();
        });
    }

    logout(error) {
        if (!error) {
            toast.success('Bye. Wish you a good day!');
            getAuth().signOut();
        }
        this.setState({
            firstname: 'Firstname',
            lastname: 'Lastname',
            email: 'E-Mail',
            imageURL: 'Photo-URL',
            isLoggedIn: false,
            jwtAccessToken: 'Access-Token',
            jwtRefreshToken: 'Refresh-Token'
        })
    }

    render() {
        const value = {
            user: this.state,
            loginUser: this.login,
            logoutUser: this.logout
        }

        return (
            <UserContext.Provider value={value}>
                {this.props.children}
            </UserContext.Provider>
        )
    }
}

export default UserProvider;
