import {Button, Row, Col, Modal, Form} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import fontawesome from '@fortawesome/fontawesome'
import {faTrash, faEdit} from "@fortawesome/free-solid-svg-icons";
import {Component} from "react";
import React from 'react';
import axios from "axios";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import moment from 'moment'
import {UserContext} from "./Providers/UserProvider";

class MeetingTasks extends Component {
    static contextType = UserContext;

    constructor(props) {
        super(props);
        fontawesome.library.add(faTrash, faEdit);
        this.due_date = 'first';
        this.show_title = false;
        this.focusRef = React.createRef();
        this.state = {
            taskId: null,
            taskName: {
                value: '',
                isValid: false,
            },
            taskDueDate: {
                value: '',
                isValid: false
            },
            taskDueDateErrorMessage: 'hey',
            cuMIsOpen: false,
            dMIsOpen: false,
            meetingTasks: [],
        }

        this.getData = this.getData.bind(this);
        this.createData = this.createData.bind(this);
        this.updateData = this.updateData.bind(this);
        this.deleteData = this.deleteData.bind(this);
        this.openCuModal = this.openCuModal.bind(this);
        this.openDModal = this.openDModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.getData();
    }

    handleChange(event) {
        this.setState({[event.target.name]: {value: event.target.value, isValid: !!event.target.value}});
    }

    openCuModal(event, task) {
        this.setState({
            taskId: task ? task.id : null,
            taskName: {value: task ? task.name : '', isValid: task ? !!task.name : false},
            taskDueDate: {
                value: task ? task.due_date : moment().format('YYYY-MM-DD'),
                isValid: true
            },
            cuMTitle: task ? 'Update task' : 'Create new task',
            cuMSubmitBtnText: task ? 'Update task' : 'Create new task',
            cuMIsOpen: true
        })
        // this.focusRef.current.focus();
    }

    openDModal(event, task) {
        this.setState({
            taskId: task ? task.id : null,
            dMIsOpen: true
        })
    }

    closeModal = () => this.setState({cuMIsOpen: false, dMIsOpen: false});

    onFormSubmit = e => {
        e.preventDefault();
        if (this.state.taskId) {
            this.updateData();
        } else {
            this.createData();
        }
    }

    async getData() {
        console.log(this.context)
        axios.get('http://localhost:8000/api/v1/education_plan/tasks', {
            headers: {
                authorization: this.context.user.jwtAccessToken
            }
        })
            .then(response => {
                this.setState({meetingTasks: response.data})
            })
            .catch(e => {
                console.log(e);
                toast.error(e);
            })
    }

    async createData() {
        axios.post('http://localhost:8000/api/v1/education_plan/tasks',
            {'name': this.state.taskName.value, 'due_date': this.state.taskDueDate.value})
            .then(response => {
                toast.success('New task created!');
            })
            .catch(e => {
                console.log(e);
                toast.error(e);
            })
            .finally(() => {
                this.getData();
                this.closeModal();
            })
    }

    async updateData() {
        axios.put(`http://localhost:8000/api/v1/education_plan/tasks/${this.state.taskId}`,
            {'name': this.state.taskName.value, 'due_date': this.state.taskDueDate.value})
            .then(response => {
                toast.success('Successfully updated task!')
            })
            .catch(e => {
                console.log(e);
                toast.error(e);
            })
            .finally(() => {
                this.getData();
                this.closeModal();
            })
    }

    async deleteData() {
        axios.delete(`http://localhost:8000/api/v1/education_plan/tasks/${this.state.taskId}`)
            .then(response => {
                toast.warning('Task deleted!');
            })
            .catch(e => {
                console.log(e);
                toast.error(e);
            })
            .finally(() => {
                this.getData();
                this.closeModal();
            })
    }

    async markTask(task, event) {
        axios.patch(`http://localhost:8000/api/v1/education_plan/tasks/${task.id}`, {'finished': !task.finished})
            .then(response => {
                task.finished ? toast.info('Task unchecked!') : toast.info('Task checked!')
            })
            .catch(e => {
                console.log(e);
                toast.error(e);
            })
            .finally(() => {
                this.getData();
            })
    }

    renderTask(task) {
        this.show_title = task.due_date !== this.due_date;
        this.due_date = task.due_date;
        return (
            <ul className={'list-group list-group-flush'} key={task.id}>
                {this.show_title && <div className={'display-6 mt-5'}>{this.due_date}</div>}
                <li className={'list-group-item border-bottom'}>
                    <input type="checkbox" className={'form-check-input me-3'} checked={task.finished}
                           onChange={(e) => this.markTask(task, e)}
                           id={`${task.id}-${task.name}`}/>
                    <label htmlFor={`${task.id}-${task.name}`}>
                        {task.finished ? <s>{task.name}</s> : <>{task.name}</>}
                    </label>
                    <span className={'float-end'}>
                        <button className={'btn btn-sm btn-primary'}
                                onClick={(e) => this.openCuModal(e, task)}>
                            <FontAwesomeIcon icon={'edit'}/>
                        </button>
                        <button className={'btn btn-sm btn-danger ms-2'}
                                onClick={(e) => this.openDModal(e, task)}>
                            <FontAwesomeIcon icon={'trash'}/>
                        </button>
                    </span>
                </li>
            </ul>
        )
    }

    render() {
        return (
            <>
                <Modal show={this.state.cuMIsOpen} onHide={this.closeModal}>
                    <Form onSubmit={this.onFormSubmit}>
                        <Modal.Header closeButton>
                            <Modal.Title>{this.state.cuMTitle}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form.Group>
                                <Form.Label>Name</Form.Label>
                                <Form.Control type={'text'}
                                              ref={this.focusRef}
                                              className={this.state.taskName.isValid ? 'form-control is-valid' : 'form-control is-invalid'}
                                              name={'taskName'}
                                              value={this.state.taskName.value || ''}
                                              placeholder={'Please name your task'}
                                              required
                                              onChange={this.handleChange}/>
                                <Form.Control.Feedback type={'invalid'}>
                                    Please type in a name for your task.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className={'mt-2'}>
                                <Form.Label>Due-Date</Form.Label>
                                <Form.Control type={'date'}
                                              ref={this.focusRef}
                                              className={this.state.taskDueDate.isValid ? 'form-control is-valid' : 'form-control is-invalid'}
                                              name={'taskDueDate'}
                                              value={this.state.taskDueDate.value || ''}
                                              onChange={this.handleChange}/>
                                <Form.Control.Feedback type={'invalid'}>
                                    Please input a valid date in the format dd.mm.yyyy
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant={'secondary'} onClick={this.closeModal}>
                                Close
                            </Button>
                            <Button variant={'primary'} type={'submit'}>
                                {this.state.cuMSubmitBtnText}
                            </Button>
                        </Modal.Footer>
                    </Form>
                </Modal>

                <Modal show={this.state.dMIsOpen} onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Delete-Task</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Do you really want to delete this task?
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant={'secondary'} onClick={this.closeModal}>
                            Close
                        </Button>
                        <Button variant={'danger'} onClick={this.deleteData}>
                            Delete task
                        </Button>
                    </Modal.Footer>
                </Modal>

                <Row>
                    <Col>
                        <div className={'display-1'}>Tasks</div>
                    </Col>
                </Row>

                <Row className={'mt-5'}>
                    <Col>
                        <Button variant={'outline-success'} onClick={this.openCuModal}>
                            New task
                        </Button>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        {this.state.meetingTasks.map(task => {
                            return this.renderTask(task);
                        })}
                    </Col>
                </Row>
            </>
        )
    }

}

export default MeetingTasks;
