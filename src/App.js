import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import './style.css'
import {BrowserRouter as Router, Switch, Route, Link, useRouteMatch, useParams} from "react-router-dom";
import {Container, Navbar, Nav, NavDropdown} from 'react-bootstrap'
import MeetingTasks from "./meeting-tasks";
import {ToastContainer} from 'react-toastify';
import Login from "./Login";
import React, {Component} from 'react';
import UserProvider, {UserContext} from './Providers/UserProvider';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <UserProvider>
                <Router>
                    <div>
                        <UserContext.Consumer>
                            {({user, logoutUser}) => {
                                return (
                                    <Navbar bg="light" expand="lg">
                                        <Container>
                                            <Navbar.Brand href="/">{user.firstname}</Navbar.Brand>
                                            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                                            <Navbar.Collapse id="basic-navbar-nav">
                                                <Nav className="me-auto">
                                                    <Nav.Link href="/">Home</Nav.Link>
                                                    <Nav.Link href="/about">About</Nav.Link>
                                                    <Nav.Link href={'/login'}>Login</Nav.Link>
                                                    <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                                                        <NavDropdown.Item href="/users">Users</NavDropdown.Item>
                                                        <NavDropdown.Item
                                                            href="/meeting-tasks">Meeting-Tasks</NavDropdown.Item>
                                                        <NavDropdown.Divider/>
                                                        <NavDropdown.Item href="/topics">Topics</NavDropdown.Item>
                                                    </NavDropdown>
                                                    <Nav.Link onClick={logoutUser}>Logout</Nav.Link>
                                                </Nav>
                                            </Navbar.Collapse>
                                        </Container>
                                    </Navbar>
                                )
                            }}
                        </UserContext.Consumer>
                        <Container className={'mt-5'}>
                            {/* A <Switch> looks trough its children <Route>s and renders the first one that matches the current URL.*/}
                            <UserContext.Consumer>
                                {({user}) => {
                                    if (!user.isLoggedIn) {
                                        return <Login/>
                                    } else {
                                        return (
                                            <Switch>
                                                <Route path="/about">
                                                    <About/>
                                                </Route>
                                                <Route path="/users">
                                                    <Users/>
                                                </Route>
                                                <Route path={'/login'}>
                                                    <Login/>
                                                </Route>
                                                <Route path={'/meeting-tasks'}>
                                                    <MeetingTasks/>
                                                </Route>
                                                <Route path={'/topics'}>
                                                    <Topics/>
                                                </Route>
                                                <Route path="/">
                                                    <Home/>
                                                </Route>
                                            </Switch>
                                        )
                                    }
                                }}
                            </UserContext.Consumer>
                        </Container>

                        {/* Toast-Container */}
                        <ToastContainer
                            theme={'colored'}
                            position={'bottom-right'}
                            autoClose={5000}
                            closeOnClick
                            draggable
                            pauseOnHover
                        />

                    </div>
                </Router>
            </UserProvider>
        )
    }

}

function Home() {
    return <h2>Home</h2>;
}

function About() {
    return <h2>About</h2>;
}

function Users() {
    return <h2>Users</h2>;
}

function Topics() {
    let match = useRouteMatch();

    return (
        <div>
            <h2>Topics</h2>

            <ul>
                <li>
                    <Link to={`${match.url}/components`}>Components</Link>
                </li>
                <li>
                    <Link to={`${match.url}/props-v-state`}>Props v. State</Link>
                </li>
            </ul>

            <Switch>
                <Route path={`${match.path}/:topicId`}>
                    <Topic/>
                </Route>
                <Route path={match.path}>
                    <h3>Please select a topic.</h3>
                </Route>
            </Switch>
        </div>
    );
}

function Topic() {
    let {topicId} = useParams();
    return <h3>Requested topic ID: {topicId}</h3>
}

export default App;
