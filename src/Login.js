import {Button, Row, Col} from "react-bootstrap";
import React, {Component} from "react";
import './style.css';
import {UserContext} from './Providers/UserProvider';

class Login extends Component {
    static contextType = UserContext;

    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <UserContext.Consumer>
                {({user}) => {
                    return (
                        <div>
                            <Row>
                                <Col>
                                    Hallo {user.firstname} {user.lastname}
                                </Col>
                                <Col>
                                    <Button variant={'outline-danger'} onClick={() => {
                                        this.context.logoutUser()
                                    }}>
                                        Logout
                                    </Button>
                                </Col>
                            </Row>
                            <Row className={'mt-5'}>
                                <Col>
                                    {!user.isLoggedIn &&
                                    <Button variant={"outline-info"} onClick={this.context.loginUser}>Test
                                        Google-Login</Button>}
                                    {user.isLoggedIn && <div>You are logged in</div>}
                                </Col>
                            </Row>
                        </div>
                    )
                }}
            </UserContext.Consumer>
        )
    }
}

export default Login;
